@extends("layouts.sports")
@section("sportContent")
<h4>BasketBall</h4>
<a class="btn btn-danger" 
onclick="event.preventDefault(); $('#delete-form').submit();">
  Clear All
</a>
<form  id="delete-form" action="{{route('sports.destroy',$activeSportKey)}}" 
  method="post"> 
@csrf @method('DELETE') 
</form> 
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">Match</th>
        <th scope="col">Player</th>
        <th scope="col">position</th>
        <th scope="col">team</th>
        <th scope="col">scored points</th>
        <th scope="col">Rebound</th>
        <th scope="col">Assists</th>
        <th scope="col">Score</th>
      </tr>



    </thead>
    <tbody>
        
      @foreach ($matchesDetails as $key => $matchDetail)
      @php
      $winPlayersObj = $winningPlayers->where('matches_id',$matchDetail->match_id)
     ->where('players_id',$matchDetail->players_id)->first();   
     $winTeamObj = $winningTeams->where('matches_id',$matchDetail->match_id)
     ->where('sports_teams_id',$matchDetail->team_id)->first(); 
     @endphp
     
      <tr>
      <td>{{$matchDetail->match_name}}</td>
      @if(!is_null($winPlayersObj) &&
      $winPlayersObj->players_id == $matchDetail->players_id)
      <td class="table-success">
      @else
      <td>
      @endif
      {{$matchDetail->player_name}}</td>
      <td>{{$matchDetail->positions}}</td>
      @if (!is_null($winTeamObj) && $winTeamObj->sports_teams_id== $matchDetail->team_id)
      <td class="table-success">
          @else
          <td>
      @endif
      {{$matchDetail->team_name}}</td>
      <td>{{$matchDetail->scored_points}}</td>
      <td>{{$matchDetail->rebounds}}</td>
      <td>{{$matchDetail->assist}}</td>
      @if (!is_null($winTeamObj) && $winTeamObj->sports_teams_id== $matchDetail->team_id)
      <td class="table-success">
          @else
          <td>
      @endif
      {{$matchDetail->score}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @endsection