@extends("layouts.main")
<div>
    <h3>Upload Matches:</h3>
    <div class="UploadContent">
      <button onclick="addNewFileUpload()" class="newMatchBtn">New Match</button>
    
    
<form id="uploadSportsForm" action="{{route("upload")}}" 
method="post" enctype="multipart/form-data">
    @csrf
    <div id="inputFiles" class="inputFiles">
    <input  type="file" name="fileToUpload" id="fileToUpload">
    </div>
    <input type="submit" class="UploadSubmitBtn" value="Upload" name="submit">
  </form>
</div>
</div>
@if($errors->any())
<div class="alert alert-danger">
@foreach ($errors->all() as $error)
{{$error}}
</br>
@endforeach
</div>
@endif
