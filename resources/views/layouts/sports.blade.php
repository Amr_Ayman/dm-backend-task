@extends("layouts.main")
@section("content")

<a class="btn btn-success" href="{{route('upload')}}">
    Upload Matches
</a>

<div class="mainContainer">
<div class="sideNav">
@foreach ($Sports as $key => $sport)

@if (isset($activeSportKey) && $activeSportKey == $sport->id)
<a class="menuItem active" href="{{route("sports",$sport->id)}}">{{$sport->name}}</a>
@else
<a class="menuItem" href="{{'../sports/'.$sport->id}}" >{{$sport->name}}</a>
@endif
    
@endforeach
</div>
<div class="sportsView">
@if (isset($activeSportKey))
    
<div class="alert alert-info">
  Winning teams and MVP players  are Highlighted
  </br>
  Scores for wining teams is increamented
</div>
@else
<div class="alert alert-info">
  Start By Uploading Matches Text Files
</div>    
@endif

@yield('sportContent')
</div>
</div>

@endSection