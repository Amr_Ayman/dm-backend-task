<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MatchPlayersWinners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_players_winners', function (Blueprint $table) {
            $table->id();
            $table->foreignId('players_id')->constrained('players');
            $table->foreignId('matches_id')->constrained('matches');
            $table->integer("score")->default(0);
            $table->timestamps();  
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_players_winners');
    }
}
