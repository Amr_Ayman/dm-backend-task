<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MatchTeamWinners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_team_winners', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sports_teams_id')->constrained('sports_teams');
            $table->foreignId('matches_id')->constrained('matches');
            $table->integer("score")->default(0);
            $table->timestamps();  
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_team_winners');
    }
}
