<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MatchesDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('players_id')->constrained('players');
            $table->foreignId('matches_id')->constrained('matches');
            $table->foreignId('sports_teams_id')->constrained('sports_teams');
            $table->text("positions")->nullable();
            $table->integer("scored_points")->default(0);
            $table->integer("rebounds")->default(0);
            $table->integer("assist")->default(0);
            $table->integer("goals_made")->default(0);
            $table->integer("goals_recieved")->default(0);
            $table->integer("score")->default(0);
            $table->timestamps();  
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches_details');
    }
}
