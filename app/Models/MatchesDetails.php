<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchesDetails extends Model
{
    use HasFactory;
    protected  $fillable = [
        'players_id', 
        'scored_points',
        'rebounds',
        'assist',
        'goals_made',
        'goals_recieved',
        'sports_teams_id',
        'positions',
        'matches_id',
        'score'
    ];
    public function players()
    {
        return $this->belongsTo(Players::class);
    }

    public function SportsTeams()
    {
        return $this->belongsTo(SportsTeams::class);
    }
    public function Matches()
    {
        return $this->belongsTo(Matches::class);
    }
}
