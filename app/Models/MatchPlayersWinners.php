<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchPlayersWinners extends Model
{
    use HasFactory;
    protected  $fillable = [
        'players_id',
        'matches_id',
        'score'
    ];
    public function Players()
    {
        return $this->belongsTo(Players::class);
    }
    public function Matches()
    {
        return $this->belongsTo(Matches::class);
    }
}
