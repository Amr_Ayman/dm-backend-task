<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MatchTeamWinners extends Model
{
    use HasFactory;
    protected  $fillable = [
        'sports_teams_id',
        'matches_id',
        'score'
    ];
    public function SportsTeams()
    {
        return $this->belongsTo(SportsTeams::class);
    }
    public function Matches()
    {
        return $this->belongsTo(Matches::class);
    }
}
