<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sports extends Model
{
    use HasFactory;
    protected  $fillable = [
        'name', 
    ];
    public function Matches()
    {
        return $this->hasMany(Matches::class);
    }
    public function PlayersSportsScores()
    {
        return $this->hasMany(PlayersSportsScores::class);
    }
    public function SportsTeams()
    {
        return $this->hasMany(SportsTeams::class);
    }
}
