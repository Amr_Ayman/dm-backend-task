<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SportsTeams extends Model
{
    use HasFactory;
    protected  $fillable = [
        'name', 
        'sports_id'
    ];
    public function Sports()
    {
        return $this->belongsTo(Sports::class);
    }
    public function MatchesDetails()
    {
        return $this->hasMany(MatchesDetails::class);
    }
    
}
