<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Players;
use App\Models\Matches;
use App\Models\MatchTeamWinners;
use App\Models\Sports;
use App\Models\MatchPlayersWinners;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;


class sportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (App::environment() != 'testing'){
            try{
               DB::select("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  'mvp'");
               Sports::all()->first();
            } catch(Exception $e){
                die("Database mvp is not found, Please create database [mvp] then run migrations first");
            }
        }
       $allSports = Sports::orderBy('id')->get();
        if(count($allSports) > 0){
            return redirect('/sports/'.$allSports->first()->id);
        }   
       return view("layouts.sports")
        ->with("Sports",$allSports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\sports  $sports
     * @return \Illuminate\Http\Response
     */
    public function show($sportsId)
    {
        $allSports = Sports::orderBy('id')->get();
        if(is_null(Sports::find($sportsId))){
            return view('exceptions')->with("msg","Sport With Id ".$sportsId." Is Not Found");
        }
        $matchesDetails = Matches::where('matches.sports_id',$sportsId)
        ->join('matches_details','matches_details.matches_id','=','matches.id')
        ->join('players','players.id','=','matches_details.players_id')
        ->join('sports_teams','sports_teams.id','=','matches_details.sports_teams_id')
        ->orderBy('matches.id')
        ->get(['matches.id as match_id','matches.name as match_name','players.name as player_name','sports_teams.id as team_id','sports_teams.name as team_name', 'matches_details.*']);
        $winningTeams = MatchTeamWinners::whereIn('matches_id',Matches::where('matches.sports_id',$sportsId)->get()->pluck('id'))->get();
        $winningPlayers = MatchPlayersWinners::whereIn('matches_id',Matches::where('matches.sports_id',$sportsId)->get()->pluck('id'))->get();

        
        return view(Sports::find($sportsId)->name)
        ->with("matchesDetails",$matchesDetails)
        ->with('Sports',$allSports)
        ->with('activeSportKey',$sportsId)
        ->with('winningTeams',$winningTeams)
        ->with('winningPlayers',$winningPlayers);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\sports  $sports
     * @return \Illuminate\Http\Response
     */
    public function edit(sports $sports)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\sports  $sports
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sports $sports)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\sports  $sports
     * @return \Illuminate\Http\Response
     */
    public function destroy($sports)
    {

        DB::beginTransaction();
       
        Sports::where('id',$sports)->delete();
        DB::commit();
        $firstSportId = Sports::orderBy('id')->first();
if(!is_null($firstSportId)){
    $firstSportId=$firstSportId->id;
}
        return redirect('/sports/'.$firstSportId);

        // $status = Sports::where('id',$sports)->delete();
        // dd($status);
    }
}
