<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Classes\HandBall;
use App\Http\Classes\BasketBall;
use App\Models\MatchesDetails;
use App\Models\Players;
use App\Models\Sports;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\SportsTeams;

class uploadFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("uploadFiles");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $classMapper = ["BASKETBALL"=>BasketBall::class, "HANDBALL" => HandBall::class]; 
        $files = $request->files;
       
        if(count($files) == 0){

            return redirect()->back()->withErrors(['Please Select At Least One Match']);
        }
        DB::beginTransaction();
            foreach ($files as $key => $matchFile) {
                $filename = $matchFile->getClientOriginalName();
                $fileContents = file_get_contents($matchFile->getRealPath());
                $lines = explode("\n", $fileContents);
                
                if(count($lines) == 1){
                    DB::rollback();
                    return redirect()->back()->withErrors(['match files must contain > 1 row']);

                }
                $sportTitle = trim($lines[0]);
                if(isset($classMapper[$sportTitle])){
                    $obj = new $classMapper[$sportTitle]();
                    $sportId = $obj->addSport();
                    $matchId = $obj->addMatch($sportId);
                    for($i = 1;$i<count($lines);$i++){
                        $line = $lines[$i];
                        $details = explode(";", $line);
                        $playerId = $this->addPlayer($matchId, $details[1], $details[0], $details[2]);
                        $isValid = $obj->validateRow($details, $filename, $sportId, $matchId);
                        if(!$isValid->status){
                            DB::rollback();
                            return redirect()->back()->withErrors([$isValid->msg]);

                        }
                        $teamId = $this->addTeam($sportId, $details[3]);
                        $obj->store($details, $matchId, $sportId, $teamId, $playerId);
                    }
                    $obj->calculate($matchId, $sportId);
                }
            }    
            DB::commit();
            $firstSportId = Sports::orderBy('id')->first()->id;
            return redirect('/sports/'.$firstSportId);
}

private function addTeam($sportId, $teamName){
    $team_name = str_replace("Team ","", $teamName);
    $teams = SportsTeams::where('sports_id',$sportId)
    ->where('name', $team_name)
    ->first();
    if(!$teams){
        $teams = SportsTeams::create([
           'sports_id' => $sportId,
           'name' => $team_name
        ]);
    }
    return $teams->id;
}

private function addPlayer($matchId, $nickName, $name, $number){
    $player = Players::where("nickname",$nickName)->first();
    if($player){
        $playerCount = MatchesDetails::where('players_id',$player->id)->where('matches_id',$matchId)->count();
        if($playerCount > 0){
            return redirect()->back()->withErrors("player ". $nickName." is repeated in the same match");    
        }
    }else{
        $player = Players::create([
            'nickname' => $nickName,
            'name' => str_replace("player","",$name),
            'number' => $number,
        ]);
    }
    return $player->id;
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
