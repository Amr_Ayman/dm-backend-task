<?php
namespace App\Http\Classes;
use App\Models\MatchesDetails;
use App\Http\Classes\SportsInterface;
use App\Models\Players;
use App\Models\Matches;
use App\Models\Sports;
use App\Models\SportsTeams;
use App\Models\PlayersMatches;
use App\Models\MatchTeamWinners;
use App\Models\MatchPlayersWinners;
use stdClass;

class HandBall implements SportsInterface{
    function __construct()
    {
        
    }
    function addSport(){
        $sport = Sports::where("name","Handball")->first();
        if(!$sport){
            $sport = Sports::create([
                'name' => "Handball",
            ]); 
        }
        return $sport->id;
    }
    function addMatch($sportId){
        $count = Matches::where('sports_id',$sportId)->count() + 1;
        $match = Matches::create([
            "name" => "Handball Match ".$count,
            "sports_id" => $sportId
        ]);
        return $match->id;
    }
    function validateRow($details, $fileName, $sportId, $matchId){
        $response = new stdClass();
        $response->status = true;
        $response->msg = "";
        
        if(count($details) != 7){
            $response->status = false;
            $response->msg = "$fileName has wrong number of fields, all matches rolledback";
            return $response;
        }
        return $response;
    }
    function store($details, $matchId, $sportId, $teamId, $playerId){
        $HandballRow = [];
        $HandballRow["Player"] = str_replace("player","",$details[0]);
        $HandballRow["Nick"] = $details[1];
        $HandballRow["Number"] = $details[2];
        $HandballRow["Team"] = str_replace("Team ","",$details[3]);
        $HandballRow["Position"] = $details[4];
        $HandballRow["goals_made"] = $details[5];
        $HandballRow["goals_recieved"] = trim($details[6]);

        $score = 0;
        if($HandballRow["Position"] == "G"){
            $score += 50 + ($HandballRow["goals_made"] * 5) - ($HandballRow["goals_recieved"] * 2);
        }else if($HandballRow["Position"] == "F"){
            $score += 20 + ($HandballRow["goals_made"] * 1) - ($HandballRow["goals_recieved"] * -1);
        }
        MatchesDetails::create([
            'players_id' => $playerId, 
            'sports_id' => $sportId,
            'goals_made' => $HandballRow["goals_made"],
            'goals_recieved' => $HandballRow["goals_recieved"],
            'sports_teams_id' =>$teamId,
            'positions' =>$HandballRow["Position"],
            'matches_id' => $matchId,
            'score' => $score
            ]);
    }
    function calculate($matchId, $sportsId){
        $matchDetails = MatchesDetails::where('matches_id',$matchId)->get();
        $teamScores =  $matchDetails->groupBy('sports_teams_id')->map(function ($row) {
            return $row->sum('score');
        });
       
        $maxTeamScore = $teamScores->max();
        foreach ($teamScores as $team => $score) {
            if($score == $maxTeamScore){
                MatchTeamWinners::create([
                    'sports_teams_id' => $team,
                    'matches_id' => $matchId,
                    'score' => $maxTeamScore
                ]);
                MatchesDetails::where('sports_teams_id', $team)
                ->where('matches_id',$matchId)
                ->increment('score', 10);    
            } 
        }

        $maxPlayerScore = $matchDetails->max('score');
        foreach ($matchDetails as $key => $value) {
            if($maxPlayerScore == $value->score){
                MatchPlayersWinners::create([
                    'players_id' => $value->players_id,
                    'matches_id' => $matchId,
                    'score' => $maxTeamScore
                ]);
            }   
        }
        
    
    }
}