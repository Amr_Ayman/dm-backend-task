<?php
namespace App\Http\Classes;
use App\Models\MatchesDetails;
use App\Http\Classes\SportsInterface;
use App\Models\Players;
use App\Models\Matches;
use App\Models\Sports;
use App\Models\SportsTeams;
use App\Models\PlayersMatches;
use App\Models\MatchTeamWinners;
use App\Models\MatchPlayersWinners;
use stdClass;

class BasketBall implements SportsInterface{
    function __construct()
    {
        
    }
    function addSport(){
        $sport = Sports::where("name","Basketball")->first();
        if(!$sport){
            $sport = Sports::create([
                'name' => "Basketball",
            ]); 
        }
        return $sport->id;
    }
    function addMatch($sportId){
        $count = Matches::where('sports_id',$sportId)->count() + 1;
        $match = Matches::create([
            "name" => "BasketBall Match ".$count,
            "sports_id" => $sportId
        ]);
        return $match->id;
    }
    function validateRow($details, $fileName, $sportId, $matchId){
        $response = new stdClass();
        $response->status = true;
        $response->msg = "";
        
        if(count($details) != 8){
            $response->status = false;
            $response->msg = "$fileName has wrong number of fields, all matches rolledback";
            return $response;
        }

        return $response;
    }
    function store($details, $matchId, $sportId, $teamId, $playerId){
        $basketBallRow = [];
        $basketBallRow["Player"] = str_replace("player","",$details[0]);
        $basketBallRow["Nick"] = $details[1];
        $basketBallRow["Number"] = $details[2];
        $basketBallRow["Team"] = str_replace("Team ","",$details[3]);
        $basketBallRow["Position"] = $details[4];
        $basketBallRow["ScoredPoint"] = $details[5];
        $basketBallRow["Rebound"] = $details[6];
        $basketBallRow["Assist"] = trim($details[7]);


        // PlayersMatches::create([
        //     'players_id' => $player->id, 
        //     'sports_id' => $sportId,
        //     'matches_id' =>$matchId 
        // ]);
        // $players_sports_count = players_sports::where([["players_id","=",$player->id],["sports_id","=",$sport->id]])->count();
        // if($players_sports_count == 0){
        //     players_sports::create([
        //         'players_id' => $player->id, 
        //         'sports_id' => $sport->id,
        //     ]);
        // }
        $score = 0;
        // dd($basketBallRow);
        // dd(($basketBallRow["ScoredPoint"]+0) * 2) + (($basketBallRow["Rebound"]+0) * 3) + (($basketBallRow["Assist"]+0) * 1));
        if($basketBallRow["Position"] == "G"){
            $score += ($basketBallRow["ScoredPoint"] * 2) + ($basketBallRow["Rebound"] * 3) + ($basketBallRow["Assist"] * 1);
        }else if($basketBallRow["Position"] == "F"){
            $score += ($basketBallRow["ScoredPoint"] * 2) + ($basketBallRow["Rebound"] * 2) + ($basketBallRow["Assist"] * 2);
        }else if($basketBallRow["Position"] == "C"){
            $score += ($basketBallRow["ScoredPoint"] * 2) + ($basketBallRow["Rebound"] * 1) + ($basketBallRow["Assist"] * 3);
        }
        
        MatchesDetails::create([
            'players_id' => $playerId, 
            'scored_points' => $basketBallRow["ScoredPoint"],
            'rebounds' => $basketBallRow["Rebound"],
            'assist' => $basketBallRow["Assist"],
            'sports_teams_id' =>$teamId,
            'positions' =>$basketBallRow["Position"],
            'matches_id' => $matchId,
            'score' => $score
            ]);
    }
    function calculate($matchId, $sportsId){
        $matchDetails = MatchesDetails::where('matches_id',$matchId)->get();
        $teamScores =  $matchDetails->groupBy('sports_teams_id')->map(function ($row) {
            return $row->sum('score');
        });
       
        $maxTeamScore = $teamScores->max();
        foreach ($teamScores as $team => $score) {
            if($score == $maxTeamScore){
                MatchTeamWinners::create([
                    'sports_teams_id' => $team,
                    'matches_id' => $matchId,
                    'score' => $maxTeamScore
                ]);
                MatchesDetails::where('sports_teams_id', $team)
                ->where('matches_id',$matchId)
                ->increment('score', 10);    
            } 
        }

        $maxPlayerScore = $matchDetails->max('score');
        foreach ($matchDetails as $key => $value) {
            if($maxPlayerScore == $value->score){
                MatchPlayersWinners::create([
                    'players_id' => $value->players_id,
                    'matches_id' => $matchId,
                    'score' => $maxTeamScore
                ]);
            }   
        }
        
    }
}