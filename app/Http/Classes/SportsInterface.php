<?php
namespace App\Http\Classes;

interface SportsInterface{
    function addSport();
    function addMatch($sportId);
    function validateRow($line, $fileName, $sportId, $matchId);
    function store($line, $matchId, $sportId, $teamId, $playerId);
    function calculate($match, $sportsId);
}