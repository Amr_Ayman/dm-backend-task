<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Models\Sports;
use App\Models\Matches;
use App\Models\MatchesDetails;
use App\Models\MatchTeamWinners;
use App\Models\Players;
use App\Models\SportsTeams;
use Tests\TestCase;

class UploadTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_upload_match()
    {
        $response = $this->call('POST', '/upload', [], [], 
        ['files' => new UploadedFile(__DIR__."/basketball.txt", "", true)]);
        $sportsId = Sports::all()->first()->id;
        $this->assertNotNull($sportsId);
        $matchId = Matches::where('sports_id',$sportsId)->first()->id;
        $this->assertNotNull($matchId);
        $winning_score = MatchTeamWinners::where('matches_id',$matchId)->get()->sum('score');
        $this->assertNotNull($winning_score);
        $this->assertEquals($winning_score, 105);
        $matchesDetails = MatchesDetails::all()->first();
        $this->assertNotNull($matchesDetails);
        $players = Players::all()->first();
        $this->assertNotNull($players);
        $sportsTeams = SportsTeams::all()->first();
        $this->assertNotNull($sportsTeams);
         $response->assertStatus(302);  

    }
}
