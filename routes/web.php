<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/sports', "sportsController",[
    'names' => [
        'index' => 'sports',
        'show' => 'sports/{id}'
    ]
]);
Route::get('/',"sportsController@index");
Route::resource('/upload', "uploadFilesController",[
    'names' => [
        'store' => 'upload',
    ]
]);